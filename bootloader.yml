default:
  tags: [builder]
  image: linaro/jenkins-amd64-debian:buster

variables:
  PUBLISH_SERVER:
    value: 'https://staging-artifacts.codelinaro.org/artifactory/codelinaro-qualcomm/'
    description: 'CodeLinaro publish server'
  JFROG_UPLOAD_DRY_RUN:
    value: 'false'
    description: 'Boolean defining upload dry-run'

  # local variables
  LK_GIT_LINARO: 'https://git.linaro.org/landing-teams/working/qualcomm/lk.git'
  LK_GCC_GIT: 'git://codeaurora.org/platform/prebuilts/gcc/linux-x86/arm/arm-eabi-4.8.git'
  QCOM_PTOOL_GIT: 'https://git.linaro.org/landing-teams/working/qualcomm/partioning_tool.git'
  QCOM_SIGNLK_GIT: 'https://git.linaro.org/landing-teams/working/qualcomm/signlk.git'
  PUB_DEST: '96boards/$MACHINE/linaro/rescue/${CI_PIPELINE_ID}'
  ABL_GIT_LINARO: 'https://git.linaro.org/landing-teams/working/qualcomm/abl.git'
  QCOM_SECTOOLS_GIT: 'https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/lt-qcom-gitlab-ci/landing-teams/working/qualcomm/sectools.git'
  ABL_CLANG_GIT: 'https://source.codeaurora.org/quic/la/platform/prebuilts/clang/host/linux-x86'
  DB_BOOT_TOOLS_GIT: 'https://git.linaro.org/landing-teams/working/qualcomm/db-boot-tools.git'
  DB_BOOT_TOOLS_DIR: 'db-boot-tools'
  BRANCH: 'master'
  TOOLCHAIN_URL_ARM: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-arm-none-eabi.tar.xz'
  TOOLCHAIN_URL_ARM64: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz'

.parallel_matrix:
  parallel:
    matrix:
      - MACHINE: [dragonboard410c,dragonboard820c,dragonboard845c,rb5]

stages:
  - fetch
  - build

# for external triggers, builders scripts are not present so fetch these before execution
fetch:
  image: curlimages/curl:latest
  stage: fetch
  script:
    - mkdir -p bootloader/$MACHINE
    - (cd bootloader/$MACHINE;curl -fsSOL $CI_BOOTLOADER_URL/-/raw/master/bootloader/$MACHINE/builders.sh;chmod +x builders.sh)
  extends: .parallel_matrix
  needs: []
  artifacts:
    paths:
      - bootloader/
    when: always
  variables:
    CI_BOOTLOADER_URL: 'https://gitlab.com/lt-qcom-gitlab-ci/bootloader/bootloader'
  rules:
    - if: $CI_PROJECT_URL !~ /bootloader/
      when: always

build:
  stage: build
  before_script: |
    set -ex

    # install gcc toolchain on db845c and rb5
    if [ "$MACHINE" = "dragonboard845c" -o "$MACHINE" = "rb5" ]; then
      if [ -z "${ARCH}" ]; then
          ARCH=arm64
      fi
      if [ "${TOOLCHAIN_ARCH}" ]; then
          ARCH="${TOOLCHAIN_ARCH}"
      fi

      toolchain_url_arm=$TOOLCHAIN_URL_ARM
      toolchain_url_arm64=$TOOLCHAIN_URL_ARM64
      toolchain_url=toolchain_url_$ARCH
      toolchain_url=${!toolchain_url}

      export tcdir=${CI_PROJECT_DIR}/srv/toolchain
      export tcbindir="${tcdir}/$(basename $toolchain_url .tar.xz)/bin"
      if [ ! -d "${tcbindir}" ]; then
          wget -q "${toolchain_url}"
          sudo mkdir -p "${tcdir}"
          sudo tar -xf "$(basename ${toolchain_url})" -C "${tcdir}"
      fi

      export PATH=$tcbindir:$PATH
    fi

    # clone db-boot-tools
    git clone --depth 1 $DB_BOOT_TOOLS_GIT -b $BRANCH $DB_BOOT_TOOLS_DIR
    export DB_BOOT_TOOLS_COMMIT=$(cd $DB_BOOT_TOOLS_DIR && git rev-parse HEAD)
  script:
    - bootloader/$MACHINE/builders.sh
  parallel:
    matrix:
      - MACHINE: [dragonboard410c,dragonboard820c,dragonboard845c,rb5]
  extends: .parallel_matrix
  artifacts:
    paths:
      - out2/MD5SUMS.txt
      - out2/HEADER.textile
  rules:
    - if: $MACHINE == 'dragonboard410c'
      variables:
        LK_GCC_REL: 'LA.BR.1.1.3.c4-01000-8x16.0'
        LK_GIT_REL_SD_RESCUE: 'release/LA.BR.1.2.7-03810-8x16.0+rescue'
        LK_GIT_REL_SD_BOOT: 'release/LA.BR.1.2.7-03810-8x16.0+sdboot'
        LK_GIT_REL_EMMC_BOOT: 'release/LA.BR.1.2.7-03810-8x16.0'
        QCOM_LINUX_FIRMWARE: 'http://releases.linaro.org/96boards/dragonboard410c/qualcomm/firmware/linux-board-support-package-r1034.2.1.zip'
        QCOM_LINUX_FIRMWARE_MD5: '25c241bfd5fb2e55e8185752d5fe92ce'
        QCOM_LINUX_FIRMWARE_LICENSE_MD5: '4d087ee0965cb059f1b2f9429e166f64'

    - if: $MACHINE == 'dragonboard820c'
      variables:
        LK_GCC_REL: 'LA.BR.1.1.3.c4-01000-8x16.0'
        LK_GIT_REL_SD_RESCUE: 'release/LA.HB.1.3.2-19600-8x96.0+rescue'
        LK_GIT_REL_UFS_BOOT: 'release/LA.HB.1.3.2-19600-8x96.0'
        QCOM_LINUX_FIRMWARE: 'http://releases.linaro.org/96boards/dragonboard820c/qualcomm/firmware/linux-board-support-package-r01700.1.zip'
        QCOM_LINUX_FIRMWARE_MD5: '587138c5e677342db9a88d5c8747ec6c'
        QCOM_LINUX_FIRMWARE_LICENSE_MD5: '4d087ee0965cb059f1b2f9429e166f64'

    - if: $MACHINE == 'dragonboard845c'
      variables:
        QCOM_LINUX_FIRMWARE: 'https://releases.linaro.org/96boards/dragonboard845c/qualcomm/firmware/RB3_firmware_20190529180356-v4.zip'
        QCOM_LINUX_FIRMWARE_MD5: 'ad69855a1275547b16d94a1b5405ac62'
        QCOM_LINUX_FIRMWARE_LICENSE_MD5: 'cbbe399f2c983ad51768f4561587f000'
        ABL_GIT_REL_LINUX: 'release/LE.UM.3.2.2.r1-02700-sdm845.0'
        ABL_GIT_REL_AOSP: 'release/LU.UM.1.2.1.r1-23200-QRB5165.0-v4-header'
        ABL_CLANG_REL: 'LA.UM.7.9.r1-07300-sm6150.0'

    - if: $MACHINE == 'rb5'
      variables:
        QCOM_LINUX_FIRMWARE: 'http://releases.linaro.org/96boards/rb5/qualcomm/firmware/RB5_firmware_20210331-v4.1.zip'
        QCOM_LINUX_FIRMWARE_MD5: '498aeeeb2603ddcf619c7e613c763791'
        QCOM_LINUX_FIRMWARE_LICENSE_MD5: 'cbbe399f2c983ad51768f4561587f000'
        ABL_GIT_REL_LINUX: 'release/LU.UM.1.2.1.r1-23200-QRB5165.0'
        ABL_GIT_REL_AOSP: 'release/LU.UM.1.2.1.r1-23200-QRB5165.0-v4-header'
        ABL_CLANG_REL: 'LA.UM.7.9.r1-07300-sm6150.0'
