#!/bin/bash

set -ex

sudo apt-get update
sudo apt-get install -y zip gdisk

# install jflog client tool, v1, used for publishing artifacts
(mkdir -p $HOME/bin && cd $HOME/bin && curl -fL https://getcli.jfrog.io | sh)

# download the firmware packages
wget -q ${QCOM_LINUX_FIRMWARE}
echo "${QCOM_LINUX_FIRMWARE_MD5}  $(basename ${QCOM_LINUX_FIRMWARE})" > MD5
md5sum -c MD5

unzip -j -d bootloaders-linux $(basename ${QCOM_LINUX_FIRMWARE}) "*/bootloaders-linux/*" "*/cdt-linux/*" "*/loaders/*"

# Get the Android compiler
git clone ${LK_GCC_GIT} --depth 1 -b ${LK_GCC_REL} android-gcc

# get the signing tools
git clone --depth 1 ${QCOM_SIGNLK_GIT}

# Build all needed flavors of LK
git clone --depth 1 ${LK_GIT_LINARO} -b ${LK_GIT_REL_SD_RESCUE} lk_sdrescue
git clone --depth 1 ${LK_GIT_LINARO} -b ${LK_GIT_REL_UFS_BOOT} lk_ufs_boot

for lk in lk_sdrescue lk_ufs_boot; do
    echo "Building LK in : $lk"
    cd $lk
    git log -1
    make -j4 msm8996 EMMC_BOOT=1 VERIFIED_BOOT=1 TOOLCHAIN_PREFIX=${CI_PROJECT_DIR}/android-gcc/bin/arm-eabi-
    mv build-msm8996/emmc_appsboot.mbn build-msm8996/emmc_appsboot_unsigned.mbn
    ../signlk/signlk.sh -i=./build-msm8996/emmc_appsboot_unsigned.mbn -o=./build-msm8996/emmc_appsboot.mbn -d
    cd -
done

BOOTLOADER_UFS_LINUX=dragonboard-820c-bootloader-ufs-linux-${CI_PIPELINE_ID}
BOOTLOADER_UFS_AOSP=dragonboard-820c-bootloader-ufs-aosp-${CI_PIPELINE_ID}

mkdir -p out/${BOOTLOADER_UFS_LINUX} out/${BOOTLOADER_UFS_AOSP}

# get LICENSE file (for Linux BSP)
unzip -j $(basename ${QCOM_LINUX_FIRMWARE}) "*/LICENSE"
echo "${QCOM_LINUX_FIRMWARE_LICENSE_MD5}  LICENSE" > MD5
md5sum -c MD5

# Create ptable and rawprogram/patch command files
git clone --depth 1 ${QCOM_PTOOL_GIT} ptool
(cd ptool && git log -1)
(mkdir ptool/linux && cd ptool/linux && python2 ${CI_PROJECT_DIR}/ptool/ptool.py -x ${CI_PROJECT_DIR}/$DB_BOOT_TOOLS_DIR/dragonboard820c/linux/partition.xml)
(mkdir ptool/aosp && cd ptool/aosp && python2 ${CI_PROJECT_DIR}/ptool/ptool.py -x ${CI_PROJECT_DIR}/$DB_BOOT_TOOLS_DIR/dragonboard820c/aosp/partition.xml)

# Empty/zero boot image file to clear boot partition
dd if=/dev/zero of=boot-erase.img bs=1024 count=1024

# bootloader_ufs_linux
cp -a LICENSE \
   $DB_BOOT_TOOLS_DIR/dragonboard820c/linux/flashall \
   lk_ufs_boot/build-msm8996/emmc_appsboot.mbn \
   bootloaders-linux/{cmnlib64.mbn,cmnlib.mbn,devcfg.mbn,hyp.mbn,keymaster.mbn,pmic.elf,rpm.mbn,sbc_1.0_8096.bin,tz.mbn,xbl.elf} \
   bootloaders-linux/prog_ufs_firehose_8996_ddr.elf \
   ptool/linux/{rawprogram,patch}?.xml \
   ptool/linux/gpt_{main,backup,both}?.bin \
   ptool/linux/zeros_*.bin \
   $DB_BOOT_TOOLS_DIR/dragonboard820c/ufs-provision_toshiba.xml \
   boot-erase.img \
   out/${BOOTLOADER_UFS_LINUX}

# bootloader_ufs_aosp
cp -a LICENSE \
   $DB_BOOT_TOOLS_DIR/dragonboard820c/aosp/flashall \
   lk_ufs_boot/build-msm8996/emmc_appsboot.mbn \
   bootloaders-linux/{cmnlib64.mbn,cmnlib.mbn,devcfg.mbn,hyp.mbn,keymaster.mbn,pmic.elf,rpm.mbn,sbc_1.0_8096.bin,tz.mbn,xbl.elf} \
   bootloaders-linux/prog_ufs_firehose_8996_ddr.elf \
   ptool/aosp/{rawprogram,patch}?.xml \
   ptool/aosp/gpt_{main,backup,both}?.bin \
   ptool/aosp/zeros_*.bin \
   $DB_BOOT_TOOLS_DIR/dragonboard820c/ufs-provision_toshiba.xml \
   boot-erase.img \
   out/${BOOTLOADER_UFS_AOSP}

# Final preparation of archives for publishing
mkdir ${CI_PROJECT_DIR}/out2
for i in ${BOOTLOADER_UFS_LINUX} \
         ${BOOTLOADER_UFS_AOSP} ; do
    (cd out/$i && md5sum * > MD5SUMS.txt)
    (cd out && zip -r ${CI_PROJECT_DIR}/out2/$i.zip $i)
done

# Create MD5SUMS file
(cd ${CI_PROJECT_DIR}/out2 && md5sum * > MD5SUMS.txt)

# Build information
cat > ${CI_PROJECT_DIR}/out2/HEADER.textile << EOF

h4. Bootloaders for Dragonboard 820c

This page provides the bootloaders packages for the Dragonboard 820c. There are several packages:
* *bootloader_ufs_linux* : includes the bootloaders and partition table (GPT) used when booting Linux images from onboard UFS
* *bootloader_ufs_aosp* : includes the bootloaders and partition table (GPT) used when booting AOSP images from onboard UFS

Build description:
* Build URL: "$CI_JOB_URL":$CI_JOB_URL
* Proprietary bootloaders are not published yet, and not available widely
* Linux proprietary bootloaders package: $(basename ${QCOM_LINUX_FIRMWARE})
* Little Kernel (LK) source code:
** "SD rescue boot":$LK_GIT_LINARO/log/?h=$(echo $LK_GIT_REL_SD_RESCUE | sed -e 's/+/\%2b/g')
** "UFS Linux boot":$LK_GIT_LINARO/log/?h=$(echo $LK_GIT_REL_UFS_BOOT | sed -e 's/+/\%2b/g')
* Tools version: "$DB_BOOT_TOOLS_COMMIT":$DB_BOOT_TOOLS_GIT/commit/?id=$DB_BOOT_TOOLS_COMMIT
* Partition table:
** "Linux":$DB_BOOT_TOOLS_GIT/tree/dragonboard820c/linux/partition.xml?id=$DB_BOOT_TOOLS_COMMIT
** "AOSP":$DB_BOOT_TOOLS_GIT/tree/dragonboard820c/aosp/partition.xml?id=$DB_BOOT_TOOLS_COMMIT
EOF

# Publish
cd ${CI_PROJECT_DIR}/out2
time ${HOME}/bin/jfrog rt u \
     --flat=false --include-dirs=true --symlinks=true --detailed-summary \
     --dry-run=${JFROG_UPLOAD_DRY_RUN} \
     --apikey ${LT_QCOM_CLO_API_KEY} \
     --url ${PUBLISH_SERVER} \
     "*" ${PUB_DEST}/
